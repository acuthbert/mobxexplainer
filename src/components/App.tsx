import React from 'react'

import './App.scss'
import appState from '../state/AppState';
import {observer} from "mobx-react";
import Profile from "./Profile";

const App = (props: any) => {

  const firstnameChanged = (firstname: string) => {
    appState.firstname = firstname;
  };

  const lastnameChanged = (lastname: string) => {
    appState.lastname = lastname;
  };

  const getRandomName = () => {
    appState.randomiseName();
  };

  console.log('rendered');

  return <div className="app">
    <h1 className="title">Your name is: {appState.fullname}</h1>
    <p>Enter your name</p>
    <input type="text" value={appState.firstname} onChange={(e) => {firstnameChanged(e.target.value)}} placeholder="Firstname" />
    <input type="text" value={appState.lastname} onChange={(e) => {lastnameChanged(e.target.value)}} placeholder="Lastname" />
    <p>Or get a new one!</p>
    <button onClick={getRandomName} >Random Name</button>

    <Profile/>
  </div>
};

export default observer(App);
