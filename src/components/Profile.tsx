import React from 'react';
import appState from '../state/AppState';
import {observer} from "mobx-react";

const Profile = () => {
  return <div style={{
    background: "peachpuff",
    border: "1px solid darkred",
    padding: "10px",
    margin: "30px",
    width: "50%"
  }}>
    <h4>Your Profile</h4>
    <dl>
      <dt>Name</dt>
      <dd>{appState.fullname}</dd>
    </dl>
  </div>
};

export default observer(Profile);
