import {action, computed, observable} from "mobx";

class AppState
{
  @observable
  firstname: string = '';

  @observable
  lastname: string = '';

  @computed get fullname() {
    return this.firstname + ' ' + this.lastname;
  }

  @action
  setName(firstname: string, lastname: string) {
    this.firstname = firstname;
    this.lastname = lastname;
  }

  randomiseName() {
    setTimeout(() => {
      let names = [
        'John',
        'Ally',
        'Simon',
        'Sam',
        'Tiago',
        'Lauren',
        'Steph',
        'Kennedy',
        'Cuthbert',
        'Wilson',
        'Holmes',
        'Doherty'
      ];

      const getName = () => {
        return names[Math.floor(Math.random() * Math.floor(names.length))];
      };

      this.setName(getName(), getName());
    }, 200);
  }
}

const state = new AppState();

export default state;
